#!/bin/bash

mkdir images
mkdir outputFiles

rm outputFiles/*
rm analytic/*
rm images/*.png

python makeDeltaFcn.py
./Tango analytic/T000.dat > tango.out
python plotDeltaFcn.py
